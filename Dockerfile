FROM docker:20.10.14-dind
RUN apk update && apk add py-pip python3-dev libffi-dev openssl-dev gcc libc-dev make openssh-client bash curl rust cargo
RUN pip install docker-compose==1.29.2
